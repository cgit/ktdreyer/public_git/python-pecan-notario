%if 0%{?fedora}
%global with_python3 1
%endif

%global srcname pecan-notario

Name:           python-%{srcname}
Version:        0.0.3
Release:        3%{?dist}
Summary:        JSON validation for Pecan with Notario
License:        BSD
URL:            https://github.com/alfredodeza/pecan-notario
Source0:        https://pypi.python.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python2-devel
BuildRequires:  pytest
BuildRequires:  python-notario
BuildRequires:  python-pecan
%if 0%{?with_python3}
BuildRequires:  python3-devel
BuildRequires:  python3-pytest
BuildRequires:  python3-notario
BuildRequires:  python3-pecan
%endif # with_python3

%description
Notario is flexible and succinct Python dictionary validator with the ability
to validate against both keys and values. Schemas are smaller and readable
representations of data being validated.

%package -n python2-%{srcname}
Summary:        %{summary}
Requires:       python-notario
Requires:       python-pecan
%{?python_provide:%python_provide python2-%{srcname}}

%description -n python2-%{srcname}
Notario is flexible and succinct Python dictionary validator with the ability
to validate against both keys and values. Schemas are smaller and readable
representations of data being validated.

%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:        %{summary}
Requires:       python3
Requires:       python3-notario
Requires:       python3-pecan

%description -n python3-%{srcname}
Notario is flexible and succinct Python dictionary validator with the ability
to validate against both keys and values. Schemas are smaller and readable
representations of data being validated.
%endif # with_python3

%prep
%autosetup -p1 -n %{srcname}-%{version}

%build
%{py2_build}

%if 0%{?with_python3}
%{py3_build}
%endif # with_python3

%install
%py2_install

%if 0%{?with_python3}
%py3_install
%endif # with_python3

%check
export PYTHONPATH=$(pwd)

py.test-%{python2_version} -v pecan_notario/tests

%if 0%{?with_python3}
py.test-%{python3_version} -v pecan_notario/tests
%endif # with_python3

%files -n python2-%{srcname}
%doc README.rst
%license LICENSE
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README.rst
%license LICENSE
%{python3_sitelib}/*
%endif # with_python3

%changelog
* Tue Mar 15 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.0.3-3
- correct license tag (rhbz#1315816)

* Tue Mar 15 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.0.3-2
- use %%global instead of %%define (rhbz#1315816)

* Mon Mar 07 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.0.3-1
- Initial package
